# Резделяемые библиотеки Jenkins
### _Методы включения библиотек в конвейер:_

1. Сценарный метод:
```
@Library("library@version")_
```
2. Декларативный метод:
```
libraries {
    lib("library")
}
```
3. Через определение функции:
```
def includeLibrary(String name) {
    library identifier: name,
        retriever: modernSCM(
            scm: [$class: "GitSCMSource",
                    credentialsId: "${env.CREDENTIAL}",
                    remote: "${env.GIT}",
                    traits: [gitBranchDiscovery()]
                ],
        libraryPath: "${env.LIB_PATH}"
    )
}
```