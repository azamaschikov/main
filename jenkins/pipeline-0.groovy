def runAnsible() {
    ansiblePlaybook(
        extraVars: [
            host_group: "${params.HOST_GROUP}"
        ],
        colorized: true,
        disableHostKeyChecking: true,
        inventory: "${env.WORKSPACE}/inventory/hosts",
        credentialsId: "43435f52-fb04-4887-8e44-33e16b12f4c9",
        playbook: "${env.WORKSPACE}/${params.PATH_PLAYBOOK}"
    )
}

def telegramSend (String message, String id) {
    withCredentials([string(credentialsId: "3f88d4d0-415d-4450-b89c-3218f2fd3d17", variable: "token")]) {
        httpRequest(
            httpMode: "POST",
            contentType: "APPLICATION_JSON",
            requestBody: "{\"chat_id\": \"${id}\", \"text\": \"${message}\", \"parse_mode\": \"markdown\"}",
            url: "https://api.telegram.org/bot${token}/sendMessage"
        )
    }
}

pipeline {

    agent any

    environment {
        ANSIBLE_PIPELINING = "true"
        ANSIBLE_HOST_KEY_CHECKING = "false"
    }

    /*
    tools {}
    */

    options {
        disableConcurrentBuilds()
        buildDiscarder(
            logRotator(
                numToKeepStr:"3",
                daysToKeepStr: "7",
                artifactDaysToKeepStr: "7",
                artifactNumToKeepStr: "3"
            )
        )
        ansiColor("gnome-terminal")
    }

    /*
    triggers {}
    */


    parameters {
        string(
            name: "PATH_PLAYBOOK",
            defaultValue: "ansible/playbook-0.yml",
            description: "Path to Ansible playbook or role:"
        )
        string(
            name: "HOST_GROUP",
            defaultValue: "test",
            description: "Host group in inventory file:"
        )
    }


    stages {
        stage("Prepare Workspace") {
            steps {
                cleanWs()
            }
        }
        
        stage("Clone Repository") {
            steps {
                checkout scm
            }
        }
        
        stage("Run Playbook") {
            steps {
                runAnsible()
            }
        }
    }

    post {
        success {
            echo "Success build"
            telegramSend("Build ${env.BUILD_NUMBER} completed for job ${JOB_BASE_NAME}.\nMore info: [here](${env.BUILD_URL})", "-1001473155821")
        }

        failure {
            echo "Failure build"
            telegramSend("Build ${env.BUILD_NUMBER} failed for job ${JOB_BASE_NAME}.\nMore info: [here](${env.BUILD_URL})", "-1001473155821")
        }

        always {
            cleanWs()
        }
    }
}

